package com.rosadiez.myapplication;
import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.Button;

import net.majorkernelpanic.streaming.Session;
import net.majorkernelpanic.streaming.SessionBuilder;
import net.majorkernelpanic.streaming.audio.AudioQuality;
import net.majorkernelpanic.streaming.gl.SurfaceView;
import net.majorkernelpanic.streaming.rtsp.RtspClient;
import net.majorkernelpanic.streaming.video.VideoQuality;

public class VisorActivity extends AppCompatActivity implements SurfaceHolder.Callback, Session.Callback, RtspClient.Callback
{
    SurfaceView m_superface_view;
    SurfaceHolder m_holder;
    Session m_session;
    RtspClient m_client;

    int m_resolucion_video_x;
    int m_resolucion_video_y;

    // -----------------------------------------------------------------------------

    private void i_establecerValoresResolucionVideo()
    {
        String calidad;

        calidad = Settings.getCalidad(this);

        if (calidad.equals(getString(R.string.calidad320x240)))
        {
            Log.d("CALIDAD", getString(R.string.calidad320x240));
            m_resolucion_video_x = 320;
            m_resolucion_video_y = 240;
        }
        else if (calidad.equals(getString(R.string.calidad352x288)))
        {
            Log.d("CALIDAD", getString(R.string.calidad352x288));
            m_resolucion_video_x = 352;
            m_resolucion_video_y = 288;
        }
        else if (calidad.equals(getString(R.string.calidad640x480)))
        {
            Log.d("CALIDAD", getString(R.string.calidad640x480));
            m_resolucion_video_x = 640;
            m_resolucion_video_y = 480;
        }
        else if (calidad.equals(getString(R.string.calidad1280x720)))
        {
            Log.d("CALIDAD", getString(R.string.calidad1280x720));
            m_resolucion_video_x = 1280;
            m_resolucion_video_y = 720;
        }
        else
        {
            m_resolucion_video_x = 640;
            m_resolucion_video_y = 480;
        }
    }

    // -----------------------------------------------------------------------------

    private void i_crearSesionCaptura()
    {
        i_establecerValoresResolucionVideo();

        m_session = SessionBuilder.getInstance()
                .setCallback(this)
                .setSurfaceView(m_superface_view)
                .setPreviewOrientation(0)
                .setContext(getApplicationContext())
                .setAudioEncoder(SessionBuilder.AUDIO_AAC)
                .setAudioQuality(new AudioQuality(8000, 16000))
                .setVideoEncoder(SessionBuilder.VIDEO_H264)
                .setVideoQuality(new VideoQuality(m_resolucion_video_x,m_resolucion_video_y,30,600000))
                .setPreviewOrientation(90)
                .build();
    }

    // -----------------------------------------------------------------------------

    private void i_crearClienteRTSP()
    {
        String username, password;
        String ip;
        int puerto;
        String canal, ruta_stream;

        m_client = new RtspClient();
        m_client.setSession(m_session);
        m_client.setCallback(this);

        username = Settings.getUsuario(this);
        password = Settings.getPassword(this);
        m_client.setCredentials(username, password);

        ip = Settings.getIP(this);
        puerto = Settings.getPuerto(this);
        m_client.setServerAddress(ip, puerto);

        canal = Settings.getCanal(this);
        ruta_stream = "/live/" + canal;
        m_client.setStreamPath(ruta_stream);
    }

    // --------------------------------------------------------------

    private void i_volver(Button button)
    {
        button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                finish();
            }
        });
    }

    // -----------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visor);

        Button button_volver;

        m_superface_view = (SurfaceView)findViewById(R.id.av_surfaceView);

        m_holder = m_superface_view.getHolder();
        m_holder.addCallback(this);

        i_crearSesionCaptura();

        i_crearClienteRTSP();

        m_client.startStream();

        button_volver = (Button) findViewById(R.id.av_volver);
        i_volver(button_volver);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        m_client.release();
        m_session.release();
        m_superface_view.getHolder().removeCallback(this);
    }

    // -----------------------------------------------------------------------------
    // Metodos de SurfaceHolder.Callback
    // -----------------------------------------------------------------------------

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder)
    {

    }

    // -----------------------------------------------------------------------------

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2)
    {

    }

    // -----------------------------------------------------------------------------

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder)
    {

    }

    // -----------------------------------------------------------------------------
    // Metodos de SurfaceHolder.Callback
    // -----------------------------------------------------------------------------

    @Override
    public void onBitrateUpdate(long bitrate)
    {

    }

    // -----------------------------------------------------------------------------

    @Override
    public void onSessionError(int reason, int streamType, Exception exception)
    {
        if (exception != null)
        {
            Log.d("ERROR", "onSessionError");
            Log.d("ERROR", exception.getMessage());
        }
    }

    // -----------------------------------------------------------------------------

    @Override
    public void onPreviewStarted()
    {
        Log.d("SESSION", "empezo la preview");
    }

    // -----------------------------------------------------------------------------

    @Override
    public void onSessionConfigured()
    {

    }

    // -----------------------------------------------------------------------------

    @Override
    public void onSessionStarted()
    {
        Log.d("SESSION", "empezo la session");
    }

    // -----------------------------------------------------------------------------

    @Override
    public void onSessionStopped()
    {

    }

    // -----------------------------------------------------------------------------
    // Metodos de RtspClient.Callback
    // -----------------------------------------------------------------------------

    @Override
    public void onRtspUpdate(int message, Exception exception)
    {
        if (exception != null)
        {
            Log.d("ERROR", "onRtspUpdate");
            Log.d("ERROR", exception.getMessage());
        }
    }
}

