package com.rosadiez.myapplication;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity
{
    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA,
            Manifest.permission.INTERNET
    };

    // --------------------------------------------------------------

    private void i_comprobarPermisos()
    {
        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED)
        {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    this,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    // --------------------------------------------------------------

    private void i_abrirPreferencias(Button button)
    {
        button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent intent;

                intent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(intent);
            }
        });
    }

    // --------------------------------------------------------------

    private void i_comenzarEmision(Button button)
    {
        button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent intent;

                intent = new Intent(getApplicationContext(), VisorActivity.class);
                startActivity(intent);
            }
        });
    }

    // --------------------------------------------------------------

    private void i_abrirAcercaDe(Button button)
    {
        button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent intent;

                intent = new Intent(getApplicationContext(), AcercaDeActivity.class);
                startActivity(intent);
            }
        });
    }

    // -----------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button_configuracion;
        Button button_comenzar_emision;
        Button button_acerca_de;

        i_comprobarPermisos();

        button_configuracion = (Button) findViewById(R.id.am_buttonConfiguracion);
        i_abrirPreferencias(button_configuracion);

        button_comenzar_emision = (Button) findViewById(R.id.am_buttonComenzar);
        i_comenzarEmision(button_comenzar_emision);

        button_acerca_de = (Button) findViewById(R.id.am_buttonAcercaDe);
        i_abrirAcercaDe(button_acerca_de);
    }
}
