package com.rosadiez.myapplication;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by RosaDiez on 05/02/2018.
 */

public class Settings
{
    // --------------------------------------------------------------

    public static String getCanal(Context context)
    {
        String canal;
        SharedPreferences shared_preferences;

        shared_preferences = PreferenceManager.getDefaultSharedPreferences(context);

        canal = shared_preferences.getString(context.getResources().getString(R.string.key_pref_canal), context.getResources().getString(R.string.canal1));

        return canal;
    }

    // --------------------------------------------------------------

    public static String getCalidad(Context context)
    {
        String calidad;
        SharedPreferences shared_preferences;

        shared_preferences = PreferenceManager.getDefaultSharedPreferences(context);

        calidad = shared_preferences.getString(context.getResources().getString(R.string.key_pref_calidad), context.getResources().getString(R.string.calidad640x480));

        return calidad;
    }

    // --------------------------------------------------------------

    public static String getIP(Context context)
    {
        String ip;
        SharedPreferences shared_preferences;

        shared_preferences = PreferenceManager.getDefaultSharedPreferences(context);

        ip = shared_preferences.getString(context.getResources().getString(R.string.key_pref_ip), "192.168.1.1");

        return ip;
    }

    // --------------------------------------------------------------

    public static int getPuerto(Context context)
    {
        String puerto;
        SharedPreferences shared_preferences;
        int puerto_int;

        shared_preferences = PreferenceManager.getDefaultSharedPreferences(context);

        puerto = shared_preferences.getString(context.getResources().getString(R.string.key_pref_puerto), context.getResources().getString(R.string.default_pref_puerto));

        puerto_int = Integer.parseInt(puerto);

        return puerto_int;
    }

    // --------------------------------------------------------------

    public static String getUsuario(Context context)
    {
        String usuario;
        SharedPreferences shared_preferences;

        shared_preferences = PreferenceManager.getDefaultSharedPreferences(context);

        usuario = shared_preferences.getString(context.getResources().getString(R.string.key_pref_usuario), "");

        return usuario;
    }

    // --------------------------------------------------------------

    public static String getPassword(Context context)
    {
        String password;
        SharedPreferences shared_preferences;

        shared_preferences = PreferenceManager.getDefaultSharedPreferences(context);

        password = shared_preferences.getString(context.getResources().getString(R.string.key_pref_password), "");

        return password;
    }
}
